/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_LED_MANAGER_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_LED_MANAGER_H_

#include <stdint.h>

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <base/macros.h>

#include "led_driver.h"
#include "pin_mux_manager.h"

namespace android {

struct LedSysfs {
  std::string name;
  std::string mux;
  std::unique_ptr<LedDriverInterface> driver_;
};

class Led {
 public:
  explicit Led(LedSysfs* led) : led_(led) {}
  ~Led() {
    if (!led_->mux.empty()) {
      // TODO(leecam): Set up pin muxing
    }
    led_->driver_.reset();
  }

  bool SetBrightness(uint32_t val) { return led_->driver_->SetBrightness(val); }

  bool GetBrightness(uint32_t* val) {
    return led_->driver_->GetBrightness(val);
  }

  bool GetMaxBrightness(uint32_t* val) {
    return led_->driver_->GetMaxBrightness(val);
  }

 private:
  LedSysfs* led_;
};

class LedManager {
 public:
  friend class LedManagerTest;
  ~LedManager();

  // Get the singleton.
  static LedManager* GetLedManager();
  static void ResetLedManager();

  // Used by the BSP to tell PMan of an sysfs led.
  bool RegisterLedSysfs(const std::string& name, const std::string& led_name);
  bool SetPinMux(const std::string& name, const std::string& mux);

  std::vector<std::string> GetLeds();
  bool HasLed(const std::string& name);

  bool RegisterDriver(std::unique_ptr<LedDriverInfoBase> driver_info);

  std::unique_ptr<Led> OpenLed(const std::string& name);

 private:
  LedManager();

  std::map<std::string, std::unique_ptr<LedDriverInfoBase>> driver_infos_;
  std::map<std::string, LedSysfs> leds_;

  DISALLOW_COPY_AND_ASSIGN(LedManager);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_LED_MANAGER_H_
