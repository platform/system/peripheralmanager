/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_H_

#include <stdint.h>

#include <memory>
#include <string>

#include <android-base/unique_fd.h>
#include <base/macros.h>
#include <peripheralmanager/constants.h>

namespace android {

// This interface must be implemented by all
// Gpio drivers.
class GpioDriverInterface {
 public:
  GpioDriverInterface() {}
  virtual ~GpioDriverInterface() {}

  // TODO(leecam): Init should have generic params.
  virtual bool Init(uint32_t index) = 0;

  virtual bool SetValue(bool val) = 0;
  virtual bool GetValue(bool* val) = 0;
  virtual bool SetActiveType(GpioActiveType type) = 0;
  virtual bool SetDirection(GpioDirection direction) = 0;
  virtual bool SetEdgeType(GpioEdgeType type) = 0;
  virtual bool GetPollingFd(::android::base::unique_fd* fd) = 0;
};

// The following is driver boilerplate.
// TODO(leecam): Abstract this into a seperate Driver class.
class GpioDriverInfoBase {
 public:
  GpioDriverInfoBase() {}
  virtual ~GpioDriverInfoBase() {}

  virtual std::string Compat() = 0;
  virtual std::unique_ptr<GpioDriverInterface> Probe() = 0;
};

template <class T, class PARAM>
class GpioDriverInfo : public GpioDriverInfoBase {
 public:
  explicit GpioDriverInfo(PARAM param) : param_(param) {}
  ~GpioDriverInfo() override {}

  std::string Compat() override { return T::Compat(); }

  std::unique_ptr<GpioDriverInterface> Probe() override {
    return std::unique_ptr<GpioDriverInterface>(new T(param_));
  }

 private:
  PARAM param_;
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_GPIO_DRIVER_H_
