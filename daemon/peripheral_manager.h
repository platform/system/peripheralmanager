/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_H_

#include <map>

#include <base/macros.h>

#include <android/os/BnPeripheralManager.h>

#include "peripheral_manager_client.h"

using android::binder::Status;
using android::os::BnPeripheralManager;

namespace android {

class PeripheralManager : public BnPeripheralManager,
                          public BnPeripheralManager::DeathRecipient {
 public:
  PeripheralManager();
  ~PeripheralManager();

  bool Init();

  // IPeripheralManager Interface
  Status GetClient(const sp<IBinder>& lifeline,
                   sp<os::IPeripheralManagerClient>* _aidl_return);

  void binderDied(const wp<IBinder>& who) override;

 private:
  bool InitHal();
  bool RegisterDrivers();

  std::map<IBinder*, sp<PeripheralManagerClient>> mapping_;

  DISALLOW_COPY_AND_ASSIGN(PeripheralManager);
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_PERIPHERAL_MANAGER_H_
