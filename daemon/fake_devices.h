/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_DAEMON_FAKE_DEVICES_H_
#define SYSTEM_PERIPHERALMANAGER_DAEMON_FAKE_DEVICES_H_

#include <string>
#include <vector>

#include "char_device.h"

namespace android {

class FakeCharDevice : public CharDeviceInterface {
 public:
  FakeCharDevice();
  ~FakeCharDevice() override;

  int Open(const char* pathname, int flags) override;
  int Close(int fd) override;
  int Ioctl(int fd, int request, void* argp) override;
  ssize_t Read(int fd, void* buf, size_t count) override;
  ssize_t Write(int fd, const void* buf, size_t count) override;
  int Poll(struct pollfd* fds, nfds_t nfds, int timeout) override;
};

class FakeDeviceFactory : public CharDeviceFactory {
 public:
  FakeDeviceFactory();
  ~FakeDeviceFactory();

  std::unique_ptr<CharDeviceInterface> NewCharDevice() override;
};

}  // namespace android

#endif  // SYSTEM_PERIPHERALMANAGER_DAEMON_FAKE_DEVICES_H_
