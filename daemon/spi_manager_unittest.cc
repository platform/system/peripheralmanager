/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>

#include <gtest/gtest.h>

#include "fake_devices.h"
#include "spi_manager.h"
#include "spi_driver_spidev.h"

namespace android {

class SpiManagerTest : public ::testing::Test {
 public:
  SpiManagerTest() {
    manager.RegisterDriver(std::unique_ptr<SpiDriverInfoBase>(
        new SpiDriverInfo<SpiDriverSpiDev, CharDeviceFactory*>(
            &device_factory)));
  }
  ~SpiManagerTest() = default;

 protected:
  FakeDeviceFactory device_factory;
  SpiManager manager;
};

TEST_F(SpiManagerTest, SimpleRegister) {
  manager.RegisterSpiDevBus("SPI0_1", 0, 1);
  std::vector<std::string> busses = manager.GetSpiDevBuses();
  ASSERT_EQ(1U, busses.size());
  ASSERT_EQ("SPI0_1", busses[0]);
}

TEST_F(SpiManagerTest, SimpleOpen) {
  manager.RegisterSpiDevBus("SPI0_1", 0, 1);
  std::unique_ptr<SpiDevice> device(manager.OpenSpiDevice("SPI0_1"));
  ASSERT_NE(nullptr, device);
  std::unique_ptr<SpiDevice> device2(manager.OpenSpiDevice("SPI0_1"));
  ASSERT_EQ(nullptr, device2);
  device.reset();
  std::unique_ptr<SpiDevice> device3(manager.OpenSpiDevice("SPI0_1"));
  ASSERT_NE(nullptr, device3);
}

TEST_F(SpiManagerTest, SetFrequency) {
  manager.RegisterSpiDevBus("SPI0_1", 0, 1);
  std::unique_ptr<SpiDevice> device(manager.OpenSpiDevice("SPI0_1"));
  ASSERT_NE(nullptr, device);

  device->SetFrequency(100);
}

}  // namespace android
