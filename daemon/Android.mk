#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

peripheralman_CommonCFlags := -Wall -Werror -Wno-unused-parameter
peripheralman_CommonCFlags += -Wno-sign-promo  # for libchrome
peripheralman_CommonCIncludes := \
  $(LOCAL_PATH)/../common \
  $(LOCAL_PATH)/../include \
  external/gtest/include \

peripheralman_CommonSharedLibraries := \
  libbinder \
  libbinderwrapper \
  libchrome \
  libutils \
  libhardware \

libperipheralman_internal_CommonSources := \
  char_device.cc \
  gpio_driver_sysfs.cc \
  gpio_manager.cc \
  i2c_driver_i2cdev.cc \
  i2c_manager.cc \
  led_driver_sysfs.cc \
  led_manager.cc \
  peripheral_manager.cc \
  peripheral_manager_client.cc \
  pin_mux_manager.cc \
  spi_driver_spidev.cc \
  spi_manager.cc \
  uart_driver_sysfs.cc \
  uart_manager.cc \

# peripheralman executable
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman
LOCAL_REQUIRED_MODULES := peripheralman.rc
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(peripheralman_CommonCIncludes)

LOCAL_STATIC_LIBRARIES := \
  libperipheralman_internal \
  libperipheralman_binder \
  peripheral_manager_hal_headers \

LOCAL_SHARED_LIBRARIES := \
  $(peripheralman_CommonSharedLibraries) \
  libbrillo \
  libbrillo-binder \

LOCAL_SRC_FILES := main.cc
LOCAL_INIT_RC := peripheralman.rc
include $(BUILD_EXECUTABLE)

# libperipheralman_internal static lib
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := libperipheralman_internal
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(peripheralman_CommonCIncludes)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include
LOCAL_STATIC_LIBRARIES := \
  libperipheralman_binder \
  peripheral_manager_hal_headers \

LOCAL_SHARED_LIBRARIES := \
  $(peripheralman_CommonSharedLibraries) \

LOCAL_SRC_FILES := $(libperipheralman_internal_CommonSources)
include $(BUILD_STATIC_LIBRARY)

# libperipheral_internal_test static lib used to test the client.
# ===============================================================

include $(CLEAR_VARS)
LOCAL_MODULE := libperipheralman_internal_test
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(peripheralman_CommonCIncludes)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include \
  $(LOCAL_PATH) \
  $(LOCAL_PATH)/../hal \

LOCAL_STATIC_LIBRARIES := \
  libperipheralman_binder \
  peripheral_manager_hal_headers \

LOCAL_SHARED_LIBRARIES := \
  libbinder \
  libbinderwrapper_test_support \
  libchrome \
  libutils \
  libhardware \

LOCAL_SRC_FILES := $(libperipheralman_internal_CommonSources) \
  fake_devices.cc \
  fake_i2c_devices.cc \

include $(BUILD_STATIC_LIBRARY)

# peripheralman unit tests
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := peripheralman_unittests
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(peripheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(peripheralman_CommonCIncludes)
LOCAL_STATIC_LIBRARIES := libperipheralman_internal \
  peripheral_manager_hal_headers \

LOCAL_SHARED_LIBRARIES := \
  libchrome \

LOCAL_SRC_FILES := \
  fake_devices.cc \
  fake_i2c_devices.cc \
  gpio_manager_unittest.cc \
  pin_mux_manager_unittest.cc \
  spi_manager_unittest.cc \

include $(BUILD_NATIVE_TEST)
