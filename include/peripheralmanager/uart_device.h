/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_UART_DEVICE_H_
#define SYSTEM_PERIPHERALMANAGER_UART_DEVICE_H_

#include <sys/cdefs.h>
#include <sys/types.h>

__BEGIN_DECLS

/// @defgroup Uart Uart device interface
/// @brief Functions to control an UART device.
///
/// These functions can be used to control an UART device.
/// @{

typedef struct BUartDevice BUartDevice;

/// Writes to a UART device.
/// @param device Pointer to the BUartDevice struct.
/// @param data Data to write.
/// @param len Size of the data to write.
/// @param bytes_written Output pointer to the number of bytes written.
/// @return 0 on success, errno on error.
int BUartDevice_write(const BUartDevice* device,
                      const void* data,
                      uint32_t len,
                      uint32_t* bytes_written);

/// Reads from a UART device.
/// @param device Pointer to the BUartDevice struct.
/// @param data Buffer to read the data into.
/// @param len Number of bytes to read.
/// @param bytes_read Output pointer to the number of bytes read.
/// @return 0 on success, errno on error.
int BUartDevice_read(const BUartDevice* device,
                     void* data,
                     uint32_t len,
                     uint32_t* bytes_read);

/// Sets the input and output speed of a UART device.
/// @param device Pointer to the BUartDevice struct.
/// @param device Uart device to configure.
/// @param baudrate Speed in baud.
/// @return 0 on success, errno on error.
int BUartDevice_setBaudrate(const BUartDevice* device, uint32_t baudrate);

/// Destroys a BUartDevice struct.
/// @param device Pointer to the BUartDevice struct.
void BUartDevice_delete(BUartDevice* device);

/// @}

__END_DECLS

#endif  // SYSTEM_PERIPHERALMANAGER_UART_DEVICE_H_
