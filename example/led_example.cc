/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>

#include <memory>

#include <base/logging.h>
#include <brillo/flag_helper.h>

#include <peripheralmanager/peripheral_manager_client.h>

int main(int argc, char* argv[]) {
  DEFINE_string(led, "", "Led to toggle");
  brillo::FlagHelper::Init(argc, argv, "Example client blinking a light.");
  logging::InitLogging(logging::LoggingSettings());

  if (FLAGS_led.empty()) {
    LOG(ERROR) << "Led name not specified.";
    return 1;
  }

  LOG(INFO) << "Blinking " << FLAGS_led;

  // Get a client to the PeripheralManager.
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  if (!client) {
    LOG(ERROR) << "Failed to connect to client";
    return 1;
  }

  // Open the LED.
  BLed* led;
  int ret = BPeripheralManagerClient_openLed(client, FLAGS_led.c_str(), &led);
  if (ret) {
    LOG(ERROR) << "Failed to open LED: " << strerror(ret);
    return 1;
  }

  uint32_t max_brightness = 0;

  // Get the maximum brightness possible.
  ret = BLed_getMaxBrightness(led, &max_brightness);
  if (ret) {
    LOG(ERROR) << "Failed to get the maximum brightness: " << strerror(ret);
    return 1;
  }

  // Toggle the output.
  BLed_setBrightness(led, 0);
  sleep(1);
  BLed_setBrightness(led, max_brightness / 2);
  sleep(1);
  BLed_setBrightness(led, max_brightness);
  sleep(1);
  BLed_setBrightness(led, max_brightness / 2);
  sleep(1);
  BLed_setBrightness(led, 0);

  // Make sure the led is turned off.
  uint32_t brightness;
  ret = BLed_getBrightness(led, &brightness);
  if (ret) {
    LOG(ERROR) << "Failed to read the brightness: " << strerror(ret);
    return 1;
  }

  if (brightness != 0) {
    LOG(ERROR) << "Failed to turn the LED off, led is at: "
               << std::to_string(brightness);
  }

  // Release the LED
  BLed_delete(led);

  // Close the connection to PeripheralManager.
  BPeripheralManagerClient_delete(client);

  LOG(INFO) << "Exiting";
  return 0;
}
