/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <base/logging.h>
#include <brillo/flag_helper.h>

#include <peripheralmanager/peripheral_manager_client.h>

int main(int argc, char* argv[]) {
  brillo::FlagHelper::Init(argc, argv, "PeripheralManager client.");
  logging::InitLogging(logging::LoggingSettings());

  // Get a client to the PeripheralManager.
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  if (!client) {
    LOG(ERROR) << "Failed to connect to client";
    return 1;
  }

  BUartDevice* uart;
  int ret = BPeripheralManagerClient_openUartDevice(client, "UART1", &uart);
  if (ret) {
    LOG(ERROR) << "Failed to open UART device";
    return 1;
  }

  ret = BUartDevice_setBaudrate(uart, 9600);

  char greeting[] = "What is your name ?\n";
  uint32_t written = 0;
  ret = BUartDevice_write(uart, greeting, strlen(greeting), &written);
  if (ret) {
    LOG(ERROR) << "failed to write: " << strerror(ret);
    return 1;
  }

  std::string name = "";

  while (name.size() < 40) {
    // If the user entered a newline, everything before is the name.
    auto first_newline = name.find(13);
    if (first_newline != std::string::npos) {
      name.resize(first_newline);
      break;
    }

    char buffer[40];
    uint32_t bytes_read = 0;

    // Read some data.
    ret = BUartDevice_read(uart, buffer, 40, &bytes_read);
    if (ret != 0 && ret != EAGAIN) {
      LOG(ERROR) << "failed to read: " << strerror(ret);
      return 1;
    }
    LOG(INFO) << "read " << bytes_read << "bytes";
    name.append(buffer, bytes_read);

    // Wait a little before reading again.
    // This should be replaced by a more efficient polling mechanism when it is
    // ready.
    sleep(2);
  }

  LOG(INFO) << "hello " << name;

  // Release the UART object.
  BUartDevice_delete(uart);

  // Close the connection to PeripheralManager.
  BPeripheralManagerClient_delete(client);

  LOG(INFO) << "Exiting";
  return 0;
}
