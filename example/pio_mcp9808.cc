/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <arpa/inet.h>
#include <unistd.h>

#include <memory>

#include <base/logging.h>
#include <brillo/flag_helper.h>

#include <peripheralmanager/peripheral_manager_client.h>

int main(int argc, char* argv[]) {
  brillo::FlagHelper::Init(argc, argv, "PeripheralManager client.");
  logging::InitLogging(logging::LoggingSettings());

  // Get a client to the PeripheralManager.
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  if (!client) {
    LOG(ERROR) << "Failed to connect to client";
    return 1;
  }

  // Open I2C bus.
  BI2cDevice* i2c_device;
  int ret =
      BPeripheralManagerClient_openI2cDevice(client, "I2C6", 0x18, &i2c_device);
  if (ret) {
    LOG(ERROR) << "Failed to open I2C: " << strerror(ret);
    return 1;
  }

  uint16_t val = 0;
  BI2cDevice_readRegWord(i2c_device, 0x05, &val);

  val = ntohs(val);
  float temp = (val >> 4) & 0xFF;
  temp += static_cast<float>(val & 0xF) / 16;
  printf("Temp: %f\n", temp);

  BI2cDevice_delete(i2c_device);

  // Close the connection to PeripheralManager.
  BPeripheralManagerClient_delete(client);

  return 0;
}
