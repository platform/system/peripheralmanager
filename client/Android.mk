#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

libperipheralman_CommonCFlags := -Wall -Werror -Wno-unused-parameter
libperipheralman_CommonCFlags += -Wno-sign-promo  # for libchrome
libperipheralman_CommonCIncludes := \
  $(LOCAL_PATH)/../common \
  $(LOCAL_PATH)/../include \

libperipheralman_CommonSharedLibraries := \
  libbinder \
  libbinderwrapper \
  libbrillo \
  libchrome \
  libutils \

# libperipheralman shared library
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := libperipheralman
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(libperipheralman_CommonCFlags)
LOCAL_C_INCLUDES := $(libperipheralman_CommonCIncludes)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/../include
LOCAL_SHARED_LIBRARIES := $(libperipheralman_CommonSharedLibraries)
LOCAL_STATIC_LIBRARIES := libperipheralman_binder
LOCAL_SRC_FILES := \
  gpio_impl.cc \
  i2c_device_impl.cc \
  led_impl.cc \
  peripheral_manager_client_impl.cc \
  spi_device_impl.cc \
  uart_device_impl.cc \
  wrapper.cc \

include $(BUILD_SHARED_LIBRARY)

# libperipheralman_tests executable
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := libperipheralman_tests
LOCAL_CPP_EXTENSION := .cc
LOCAL_CFLAGS := $(libperipheralman_CommonCFlags) -Wno-sign-compare
LOCAL_C_INCLUDES := $(libperipheralman_CommonCIncludes)
LOCAL_STATIC_LIBRARIES := libgtest libBionicGtestMain \
  libperipheralman_binder \
  libperipheralman_internal_test \
  libgmock \

LOCAL_SHARED_LIBRARIES := \
  $(libperipheralman_CommonSharedLibraries) \
  libperipheralman \
  libbinderwrapper_test_support \

LOCAL_SRC_FILES := \
  gpio_unittest.cc \
  i2c_unittest.cc \
  led_unittest.cc \
  peripheral_manager_client_unittest.cc \
  spi_unittest.cc \
  uart_unittest.cc \

include $(BUILD_NATIVE_TEST)
