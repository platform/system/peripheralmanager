/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "peripheral_manager_client_impl.h"

#include <android/os/IPeripheralManager.h>
#include <binderwrapper/binder_wrapper.h>

using android::binder::Status;

bool PeripheralManagerClientImpl::Init() {
  auto wrapper = android::interface_cast<android::os::IPeripheralManager>(
      android::BinderWrapper::GetOrCreateInstance()->GetService(
          "android.os.IPeripheralManager"));
  lifeline_ = new android::BBinder;

  return wrapper->GetClient(lifeline_, &client_).isOk();
}

int PeripheralManagerClientImpl::OpenGpio(const std::string& name,
                                          std::unique_ptr<GpioImpl>* gpio) {
  Status status = client_->OpenGpio(name);
  if (status.isOk()) {
    gpio->reset(new GpioImpl(name, client_));
  }
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::ListGpio(std::vector<std::string>* gpios) {
  Status status = client_->ListGpio(gpios);
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::OpenSpiDevice(
    const std::string& name,
    std::unique_ptr<SpiDeviceImpl>* device) {
  Status status = client_->OpenSpiDevice(name);
  if (status.isOk()) {
    device->reset(new SpiDeviceImpl(name, client_));
  }
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::ListSpiBuses(std::vector<std::string>* buses) {
  Status status = client_->ListSpiBuses(buses);
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::ListLeds(std::vector<std::string>* leds) {
  return client_->ListLeds(leds).serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::OpenLed(const std::string& name,
                                         std::unique_ptr<LedImpl>* led) {
  Status status = client_->OpenLed(name);
  if (status.isOk()) {
    led->reset(new LedImpl(name, client_));
  }
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::OpenI2cDevice(
    const std::string& name,
    uint32_t address,
    std::unique_ptr<I2cDeviceImpl>* device) {
  Status status = client_->OpenI2cDevice(name, address);
  if (status.isOk()) {
    device->reset(new I2cDeviceImpl(name, address, client_));
  }
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::ListI2cBuses(std::vector<std::string>* buses) {
  return client_->ListI2cBuses(buses).serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::OpenUartDevice(
    const std::string& name, std::unique_ptr<UartDeviceImpl>* device) {
  Status status = client_->OpenUartDevice(name);
  if (status.isOk()) {
    device->reset(new UartDeviceImpl(name, client_));
  }
  return status.serviceSpecificErrorCode();
}

int PeripheralManagerClientImpl::ListUartDevices(
    std::vector<std::string>* list) {
  return client_->ListUartDevices(list).serviceSpecificErrorCode();
}
