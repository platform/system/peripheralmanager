/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <utils/StrongPointer.h>
#include <memory>

#include "gpio_impl.h"
#include "led_impl.h"
#include "peripheral_manager_client_impl.h"
#include "peripheralmanager/peripheral_manager_client.h"
#include "spi_device_impl.h"
#include "uart_device_impl.h"

namespace {

// This is horrible. Get rid of it!!!
static char** ConvertStringVectorToC(const std::vector<std::string>& strings) {
  char** c_strings =
      static_cast<char**>(malloc(strings.size() * sizeof(*c_strings)));
  for (size_t i = 0; i < strings.size(); i++) {
    c_strings[i] = static_cast<char*>(malloc(strings[i].size() + 1));
    memset(c_strings[i], 0, strings[i].size() + 1);
    strcpy(c_strings[i], strings[i].c_str());
  }
  return c_strings;
}

}  // namespace

struct BPeripheralManagerClient {
  PeripheralManagerClientImpl* impl;
};

struct BGpio {
  GpioImpl* impl;
};

struct BSpiDevice {
  SpiDeviceImpl* impl;
};

struct BLed {
  LedImpl* impl;
};

struct BI2cDevice {
  I2cDeviceImpl* impl;
};

struct BUartDevice {
  UartDeviceImpl* impl;
};

BPeripheralManagerClient* BPeripheralManagerClient_new() {
  std::unique_ptr<PeripheralManagerClientImpl> impl(
      new PeripheralManagerClientImpl);
  if (impl->Init()) {
    return new BPeripheralManagerClient{impl.release()};
  }

  return NULL;
}

void BPeripheralManagerClient_delete(BPeripheralManagerClient* client) {
  delete client->impl;
  delete client;
}

// GPIO API
char** BPeripheralManagerClient_listGpio(const BPeripheralManagerClient* client,
                                         int* num_gpio) {
  std::vector<std::string> gpios;
  client->impl->ListGpio(&gpios);
  *num_gpio = gpios.size();
  return ConvertStringVectorToC(gpios);
}

int BPeripheralManagerClient_openGpio(const BPeripheralManagerClient* client,
                                      const char* name,
                                      BGpio** gpio) {
  std::unique_ptr<GpioImpl> tmp;
  int ret = client->impl->OpenGpio(name, &tmp);
  if (!ret) {
    *gpio = new BGpio{tmp.release()};
  }
  return ret;
}

int BGpio_setDirection(const BGpio* gpio, int direction) {
  android::GpioDirection dir;
  if (!DirectionFromInt(direction, &dir))
    return EINVAL;

  return gpio->impl->SetDirection(dir);
}

int BGpio_setEdgeTriggerType(const BGpio* gpio, int type) {
  android::GpioEdgeType t;
  if (!EdgeTypeFromInt(type, &t))
    return EINVAL;

  return gpio->impl->SetEdgeTriggerType(t);
}

int BGpio_setActiveType(const BGpio* gpio, int type) {
  android::GpioActiveType t;
  if (!ActiveTypeFromInt(type, &t))
    return EINVAL;

  return gpio->impl->SetActiveType(t);
}

int BGpio_setValue(const BGpio* gpio, int value) {
  return gpio->impl->SetValue(value);
}

int BGpio_getValue(const BGpio* gpio, int* value) {
  return gpio->impl->GetValue(value);
}

int BGpio_getPollingFd(const BGpio* gpio, int* fd) {
  return gpio->impl->GetPollingFd(fd);
}

int BGpio_ackInterruptEvent(int fd) {
  uint8_t buf[2];
  lseek(fd, 0, SEEK_SET);
  read(fd, buf, 2);
  return 0;
}

void BGpio_delete(BGpio* gpio) {
  delete gpio->impl;
  delete gpio;
}

// SPI API
char** BPeripheralManagerClient_listSpiBuses(
    const BPeripheralManagerClient* client,
    int* num_spi_buses) {
  std::vector<std::string> list;
  client->impl->ListSpiBuses(&list);
  *num_spi_buses = list.size();
  return ConvertStringVectorToC(list);
}

int BPeripheralManagerClient_openSpiDevice(
    const BPeripheralManagerClient* client,
    const char* name,
    BSpiDevice** dev) {
  std::unique_ptr<SpiDeviceImpl> impl;
  int ret = client->impl->OpenSpiDevice(name, &impl);
  if (impl) {
    *dev = new BSpiDevice{impl.release()};
  }

  return ret;
}

int BSpiDevice_writeByte(const BSpiDevice* device, uint8_t val) {
  return device->impl->WriteByte(val);
}

int BSpiDevice_writeBuffer(const BSpiDevice* device,
                           const void* data,
                           size_t len) {
  return device->impl->WriteBuffer(static_cast<const uint8_t*>(data), len);
}

int BSpiDevice_transfer(const BSpiDevice* device,
                        const void* tx_data,
                        void* rx_data,
                        size_t len) {
  return device->impl->Transfer(static_cast<const uint8_t*>(tx_data),
                                static_cast<uint8_t*>(rx_data), len);
}

int BSpiDevice_setFrequency(const BSpiDevice* device, uint32_t freq_hz) {
  return device->impl->SetFrequency(freq_hz);
}

int BSpiDevice_setMode(const BSpiDevice* device, int mode) {
  if (mode == SPI_MODE0 || mode == SPI_MODE1 || mode == SPI_MODE2 ||
      mode == SPI_MODE3) {
    return device->impl->SetMode(mode);
  }
  return EINVAL;
}

int BSpiDevice_setBitJustification(const BSpiDevice* device,
                                   int bit_justification) {
  if (bit_justification == SPI_LSB_FIRST ||
      bit_justification == SPI_MSB_FIRST) {
    return device->impl->SetBitJustification(bit_justification);
  }
  return EINVAL;
}

int BSpiDevice_setBitsPerWord(const BSpiDevice* device, uint8_t bits_per_word) {
  return device->impl->SetBitsPerWord(bits_per_word);
}

int BSpiDevice_setDelay(const BSpiDevice* device, uint16_t delay_usecs) {
  return device->impl->SetDelay(delay_usecs);
}

void BSpiDevice_delete(BSpiDevice* device) {
  delete device->impl;
  delete device;
}

char** BPeripheralManagerClient_listLeds(const BPeripheralManagerClient* client,
                                         int* num_leds) {
  std::vector<std::string> list;
  client->impl->ListLeds(&list);
  *num_leds = list.size();
  return ConvertStringVectorToC(list);
}

int BPeripheralManagerClient_openLed(const BPeripheralManagerClient* client,
                                     const char* name,
                                     BLed** led) {
  std::unique_ptr<LedImpl> impl;
  int ret = client->impl->OpenLed(name, &impl);
  if (!ret)
    *led = new BLed{impl.release()};

  return ret;
}

int BLed_setBrightness(const BLed* led, uint32_t brightness) {
  return led->impl->SetBrightness(brightness);
}

int BLed_getBrightness(const BLed* led, uint32_t* brightness) {
  return led->impl->GetBrightness(brightness);
}

int BLed_getMaxBrightness(const BLed* led, uint32_t* max_brightness) {
  return led->impl->GetMaxBrightness(max_brightness);
}

void BLed_delete(BLed* led) {
  delete led->impl;
  delete led;
}

// I2C API
char** BPeripheralManagerClient_listI2cBuses(
    const BPeripheralManagerClient* client, int* num_i2c_buses) {
  std::vector<std::string> list;
  client->impl->ListI2cBuses(&list);
  *num_i2c_buses = list.size();
  return ConvertStringVectorToC(list);
}

int BPeripheralManagerClient_openI2cDevice(
    const BPeripheralManagerClient* client,
    const char* name,
    uint32_t address,
    BI2cDevice** dev) {
  std::unique_ptr<I2cDeviceImpl> impl;
  int ret = client->impl->OpenI2cDevice(name, address, &impl);
  if (ret == 0) {
    *dev = new BI2cDevice{impl.release()};
  }

  return ret;
}

int BI2cDevice_read(const BI2cDevice* device,
                    void* data,
                    uint32_t len,
                    uint32_t* bytes_read) {
  return device->impl->Read(data, len, bytes_read);
}

int BI2cDevice_readRegByte(const BI2cDevice* device,
                           uint8_t reg,
                           uint8_t* val) {
  return device->impl->ReadRegByte(reg, val);
}

int BI2cDevice_readRegWord(const BI2cDevice* device,
                           uint8_t reg,
                           uint16_t* val) {
  return device->impl->ReadRegWord(reg, val);
}

int BI2cDevice_readRegBuffer(const BI2cDevice* device,
                             uint8_t reg,
                             void* data,
                             uint32_t len,
                             uint32_t* bytes_read) {
  return device->impl->ReadRegBuffer(reg, data, len, bytes_read);
}

int BI2cDevice_write(const BI2cDevice* device,
                     const void* data,
                     uint32_t len,
                     uint32_t* bytes_written) {
  return device->impl->Write(data, len, bytes_written);
}

int BI2cDevice_writeRegByte(const BI2cDevice* device,
                            uint8_t reg,
                            uint8_t val) {
  return device->impl->WriteRegByte(reg, val);
}

int BI2cDevice_writeRegWord(const BI2cDevice* device,
                            uint8_t reg,
                            uint16_t val) {
  return device->impl->WriteRegWord(reg, val);
}

int BI2cDevice_writeRegBuffer(const BI2cDevice* device,
                              uint8_t reg,
                              const void* data,
                              uint32_t len,
                              uint32_t* bytes_written) {
  return device->impl->WriteRegBuffer(reg, data, len, bytes_written);
}

void BI2cDevice_delete(BI2cDevice* device) {
  delete device->impl;
  delete device;
}

char** BPeripheralManagerClient_listUartDevices(
    const BPeripheralManagerClient* client, int* num_uart_devices) {
  std::vector<std::string> list;
  client->impl->ListUartDevices(&list);
  *num_uart_devices = list.size();
  return ConvertStringVectorToC(list);
}

int BPeripheralManagerClient_openUartDevice(
    const BPeripheralManagerClient* client,
    const char* name,
    BUartDevice** device) {
  std::unique_ptr<UartDeviceImpl> impl;
  int ret = client->impl->OpenUartDevice(name, &impl);
  if (ret == 0) {
    *device = new BUartDevice{impl.release()};
  }
  return ret;
}

int BUartDevice_setBaudrate(const BUartDevice* device, uint32_t baudrate) {
  return device->impl->SetBaudrate(baudrate);
}

int BUartDevice_write(const BUartDevice* device,
                      const void* data,
                      uint32_t size,
                      uint32_t* bytes_written) {
  return device->impl->Write(data, size, bytes_written);
}

int BUartDevice_read(const BUartDevice* device,
                     void* data,
                     uint32_t size,
                     uint32_t* bytes_read) {
  return device->impl->Read(data, size, bytes_read);
}

void BUartDevice_delete(BUartDevice* device) {
  delete device->impl;
  delete device;
}
