/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "uart_device_impl.h"

#include <binder/Status.h>

using android::binder::Status;

UartDeviceImpl::UartDeviceImpl(
    const std::string& name,
    android::sp<android::os::IPeripheralManagerClient> client)
    : name_(name), client_(client) {}

UartDeviceImpl::~UartDeviceImpl() {
  client_->ReleaseUartDevice(name_);
}

int UartDeviceImpl::SetBaudrate(uint32_t baudrate) {
  return client_->SetUartDeviceBaudrate(name_, baudrate)
      .serviceSpecificErrorCode();
}

int UartDeviceImpl::Write(const void* data,
                          uint32_t size,
                          uint32_t* bytes_written) {
  const uint8_t* d = reinterpret_cast<const uint8_t*>(data);
  return client_
      ->UartDeviceWrite(name_,
                        std::vector<uint8_t>(d, d + size),
                        reinterpret_cast<int32_t*>(bytes_written))
      .serviceSpecificErrorCode();
}

int UartDeviceImpl::Read(void* data, uint32_t size, uint32_t* bytes_read) {
  std::vector<uint8_t> v;
  Status status = client_->UartDeviceRead(
      name_, &v, size, reinterpret_cast<int32_t*>(bytes_read));
  if (status.isOk()) {
    memcpy(data, v.data(), *bytes_read);
  }

  return status.serviceSpecificErrorCode();
}
