/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SYSTEM_PERIPHERALMANAGER_SPI_DEVICE_IMPL_H_
#define SYSTEM_PERIPHERALMANAGER_SPI_DEVICE_IMPL_H_

#include <string>

#include <android/os/IPeripheralManagerClient.h>
#include <utils/StrongPointer.h>

class SpiDeviceImpl {
 public:
  SpiDeviceImpl(const std::string& bus,
                android::sp<android::os::IPeripheralManagerClient> client);
  ~SpiDeviceImpl();

  int WriteByte(uint8_t byte);

  int WriteBuffer(const uint8_t* data, size_t len);

  int Transfer(const uint8_t* tx_data, uint8_t* rx_data, size_t len);

  int SetFrequency(uint32_t speed_hz);

  int SetMode(int mode);

  int SetBitJustification(int bit_justification);

  int SetBitsPerWord(uint32_t bits_per_word);

  int SetDelay(uint16_t delay_usecs);

 private:
  std::string name_;
  android::sp<android::os::IPeripheralManagerClient> client_;
};

#endif  // SYSTEM_PERIPHERALMANAGER_SPI_DEVICE_IMPL_H_
