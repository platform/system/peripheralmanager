/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include <binderwrapper/binder_test_base.h>
#include <binderwrapper/stub_binder_wrapper.h>
#include <gtest/gtest.h>

#include "fake_devices.h"
#include "peripheral_manager.h"
#include "peripheralmanager/peripheral_manager_client.h"
#include "peripheralmanager/uart_device.h"
#include "uart_driver_sysfs.h"

using android::CharDeviceFactory;
using android::FakeDeviceFactory;
using android::UartDriverInfo;
using android::UartDriverInfoBase;
using android::UartDriverSysfs;
using android::UartManager;
using android::PeripheralManager;

// Base class used to test the Uart C API.
// As we rely on static, global managers, we cannot run this tests in parallel.
// Please use -j1 when running theses tests or you may see false negatives.
class UartTest : public android::BinderTestBase {
 public:
  void SetUp() {
    android::sp<PeripheralManager> pman(new PeripheralManager);
    android::String8 interface_desc(pman->getInterfaceDescriptor());
    binder_wrapper()->SetBinderForService(interface_desc.string(), pman);

    UartManager* man = UartManager::GetManager();

    man->RegisterDriver(std::unique_ptr<UartDriverInfoBase>(
        new UartDriverInfo<UartDriverSysfs, CharDeviceFactory*>(
            &device_factory_)));

    man->RegisterUartDevice("UART0", "/dev/ttyuart0");
  }

  void TearDown() { UartManager::ResetManager(); }

 private:
  FakeDeviceFactory device_factory_;
};

// Test that we can list the available devices.
TEST_F(UartTest, ListDevices) {
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  int n = 0;
  char** list = BPeripheralManagerClient_listUartDevices(client, &n);

  ASSERT_EQ(1, n);
  ASSERT_EQ("UART0", std::string(list[0]));

  free(list[0]);
  free(list);

  BPeripheralManagerClient_delete(client);
}

// Test that we can open a uart device and set the baudrate.
TEST_F(UartTest, OpenDevice) {
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  BUartDevice* device;
  ASSERT_EQ(0,
            BPeripheralManagerClient_openUartDevice(client, "UART0", &device));

  // Can set a valid baudrate.
  ASSERT_EQ(0, BUartDevice_setBaudrate(device, 115200));

  // Setting an invalid baudrate should return an error.
  ASSERT_EQ(EINVAL, BUartDevice_setBaudrate(device, 12345));

  BUartDevice_delete(device);
  BPeripheralManagerClient_delete(client);
}

// Test that we can detect when a device is accessed twice and report an
// appropriate error.
TEST_F(UartTest, HandlesConflict) {
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  BUartDevice* device_a;
  BUartDevice* device_b;

  // Can open UART0 once.
  ASSERT_EQ(
      0, BPeripheralManagerClient_openUartDevice(client, "UART0", &device_a));

  // Can't open it twice.
  ASSERT_EQ(
      EBUSY,
      BPeripheralManagerClient_openUartDevice(client, "UART0", &device_b));

  // Can open it again once closed.
  BUartDevice_delete(device_a);
  ASSERT_EQ(
      0, BPeripheralManagerClient_openUartDevice(client, "UART0", &device_b));

  BUartDevice_delete(device_b);
  BPeripheralManagerClient_delete(client);
}

// Test that we report the right error when trying to open an unknown device.
TEST_F(UartTest, UnknownDevice) {
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  BUartDevice* device;
  ASSERT_EQ(
      ENODEV,
      BPeripheralManagerClient_openUartDevice(client, "unknown", &device));

  BPeripheralManagerClient_delete(client);
}

// Test that we can read and write to a device.
TEST_F(UartTest, ReadWrite) {
  BPeripheralManagerClient* client = BPeripheralManagerClient_new();

  BUartDevice* device;
  ASSERT_EQ(0,
            BPeripheralManagerClient_openUartDevice(client, "UART0", &device));

  char buf[40];
  uint32_t nwritten;
  EXPECT_EQ(0, BUartDevice_write(device, buf, 40, &nwritten));

  uint32_t nread;
  EXPECT_EQ(0, BUartDevice_read(device, buf, 40, &nread));

  BUartDevice_delete(device);
  BPeripheralManagerClient_delete(client);
}
